package com.android.bluetooth.btservice.storage;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class MetadataDatabase_Impl extends MetadataDatabase {
  private volatile MetadataDao _metadataDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(102) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `metadata` (`address` TEXT NOT NULL, `migrated` INTEGER NOT NULL, `a2dpSupportsOptionalCodecs` INTEGER NOT NULL, `a2dpOptionalCodecsEnabled` INTEGER NOT NULL, `a2dp_priority` INTEGER, `a2dp_sink_priority` INTEGER, `hfp_priority` INTEGER, `hfp_client_priority` INTEGER, `hid_host_priority` INTEGER, `pan_priority` INTEGER, `pbap_priority` INTEGER, `pbap_client_priority` INTEGER, `map_priority` INTEGER, `sap_priority` INTEGER, `hearing_aid_priority` INTEGER, `map_client_priority` INTEGER, `manufacturer_name` BLOB, `model_name` BLOB, `software_version` BLOB, `hardware_version` BLOB, `companion_app` BLOB, `main_icon` BLOB, `is_untethered_headset` BLOB, `untethered_left_icon` BLOB, `untethered_right_icon` BLOB, `untethered_case_icon` BLOB, `untethered_left_battery` BLOB, `untethered_right_battery` BLOB, `untethered_case_battery` BLOB, `untethered_left_charging` BLOB, `untethered_right_charging` BLOB, `untethered_case_charging` BLOB, `enhanced_settings_ui_uri` BLOB, PRIMARY KEY(`address`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"fd763bdc49c947fc2b87c89531559d53\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `metadata`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMetadata = new HashMap<String, TableInfo.Column>(33);
        _columnsMetadata.put("address", new TableInfo.Column("address", "TEXT", true, 1));
        _columnsMetadata.put("migrated", new TableInfo.Column("migrated", "INTEGER", true, 0));
        _columnsMetadata.put("a2dpSupportsOptionalCodecs", new TableInfo.Column("a2dpSupportsOptionalCodecs", "INTEGER", true, 0));
        _columnsMetadata.put("a2dpOptionalCodecsEnabled", new TableInfo.Column("a2dpOptionalCodecsEnabled", "INTEGER", true, 0));
        _columnsMetadata.put("a2dp_priority", new TableInfo.Column("a2dp_priority", "INTEGER", false, 0));
        _columnsMetadata.put("a2dp_sink_priority", new TableInfo.Column("a2dp_sink_priority", "INTEGER", false, 0));
        _columnsMetadata.put("hfp_priority", new TableInfo.Column("hfp_priority", "INTEGER", false, 0));
        _columnsMetadata.put("hfp_client_priority", new TableInfo.Column("hfp_client_priority", "INTEGER", false, 0));
        _columnsMetadata.put("hid_host_priority", new TableInfo.Column("hid_host_priority", "INTEGER", false, 0));
        _columnsMetadata.put("pan_priority", new TableInfo.Column("pan_priority", "INTEGER", false, 0));
        _columnsMetadata.put("pbap_priority", new TableInfo.Column("pbap_priority", "INTEGER", false, 0));
        _columnsMetadata.put("pbap_client_priority", new TableInfo.Column("pbap_client_priority", "INTEGER", false, 0));
        _columnsMetadata.put("map_priority", new TableInfo.Column("map_priority", "INTEGER", false, 0));
        _columnsMetadata.put("sap_priority", new TableInfo.Column("sap_priority", "INTEGER", false, 0));
        _columnsMetadata.put("hearing_aid_priority", new TableInfo.Column("hearing_aid_priority", "INTEGER", false, 0));
        _columnsMetadata.put("map_client_priority", new TableInfo.Column("map_client_priority", "INTEGER", false, 0));
        _columnsMetadata.put("manufacturer_name", new TableInfo.Column("manufacturer_name", "BLOB", false, 0));
        _columnsMetadata.put("model_name", new TableInfo.Column("model_name", "BLOB", false, 0));
        _columnsMetadata.put("software_version", new TableInfo.Column("software_version", "BLOB", false, 0));
        _columnsMetadata.put("hardware_version", new TableInfo.Column("hardware_version", "BLOB", false, 0));
        _columnsMetadata.put("companion_app", new TableInfo.Column("companion_app", "BLOB", false, 0));
        _columnsMetadata.put("main_icon", new TableInfo.Column("main_icon", "BLOB", false, 0));
        _columnsMetadata.put("is_untethered_headset", new TableInfo.Column("is_untethered_headset", "BLOB", false, 0));
        _columnsMetadata.put("untethered_left_icon", new TableInfo.Column("untethered_left_icon", "BLOB", false, 0));
        _columnsMetadata.put("untethered_right_icon", new TableInfo.Column("untethered_right_icon", "BLOB", false, 0));
        _columnsMetadata.put("untethered_case_icon", new TableInfo.Column("untethered_case_icon", "BLOB", false, 0));
        _columnsMetadata.put("untethered_left_battery", new TableInfo.Column("untethered_left_battery", "BLOB", false, 0));
        _columnsMetadata.put("untethered_right_battery", new TableInfo.Column("untethered_right_battery", "BLOB", false, 0));
        _columnsMetadata.put("untethered_case_battery", new TableInfo.Column("untethered_case_battery", "BLOB", false, 0));
        _columnsMetadata.put("untethered_left_charging", new TableInfo.Column("untethered_left_charging", "BLOB", false, 0));
        _columnsMetadata.put("untethered_right_charging", new TableInfo.Column("untethered_right_charging", "BLOB", false, 0));
        _columnsMetadata.put("untethered_case_charging", new TableInfo.Column("untethered_case_charging", "BLOB", false, 0));
        _columnsMetadata.put("enhanced_settings_ui_uri", new TableInfo.Column("enhanced_settings_ui_uri", "BLOB", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMetadata = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMetadata = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMetadata = new TableInfo("metadata", _columnsMetadata, _foreignKeysMetadata, _indicesMetadata);
        final TableInfo _existingMetadata = TableInfo.read(_db, "metadata");
        if (! _infoMetadata.equals(_existingMetadata)) {
          throw new IllegalStateException("Migration didn't properly handle metadata(com.android.bluetooth.btservice.storage.Metadata).\n"
                  + " Expected:\n" + _infoMetadata + "\n"
                  + " Found:\n" + _existingMetadata);
        }
      }
    }, "fd763bdc49c947fc2b87c89531559d53", "5281f052330207f50e312bb97bcde564");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "metadata");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `metadata`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected MetadataDao mMetadataDao() {
    if (_metadataDao != null) {
      return _metadataDao;
    } else {
      synchronized(this) {
        if(_metadataDao == null) {
          _metadataDao = new MetadataDao_Impl(this);
        }
        return _metadataDao;
      }
    }
  }
}
