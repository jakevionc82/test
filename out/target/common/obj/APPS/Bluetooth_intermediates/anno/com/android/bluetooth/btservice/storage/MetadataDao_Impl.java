package com.android.bluetooth.btservice.storage;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.sqlite.db.SupportSQLiteStatement;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public final class MetadataDao_Impl implements MetadataDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfMetadata;

  private final SharedSQLiteStatement __preparedStmtOfDelete;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAll;

  public MetadataDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMetadata = new EntityInsertionAdapter<Metadata>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `metadata`(`address`,`migrated`,`a2dpSupportsOptionalCodecs`,`a2dpOptionalCodecsEnabled`,`a2dp_priority`,`a2dp_sink_priority`,`hfp_priority`,`hfp_client_priority`,`hid_host_priority`,`pan_priority`,`pbap_priority`,`pbap_client_priority`,`map_priority`,`sap_priority`,`hearing_aid_priority`,`map_client_priority`,`manufacturer_name`,`model_name`,`software_version`,`hardware_version`,`companion_app`,`main_icon`,`is_untethered_headset`,`untethered_left_icon`,`untethered_right_icon`,`untethered_case_icon`,`untethered_left_battery`,`untethered_right_battery`,`untethered_case_battery`,`untethered_left_charging`,`untethered_right_charging`,`untethered_case_charging`,`enhanced_settings_ui_uri`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Metadata value) {
        if (value.getAddress() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getAddress());
        }
        final int _tmp;
        _tmp = value.migrated ? 1 : 0;
        stmt.bindLong(2, _tmp);
        stmt.bindLong(3, value.a2dpSupportsOptionalCodecs);
        stmt.bindLong(4, value.a2dpOptionalCodecsEnabled);
        final ProfilePrioritiesEntity _tmpProfilePriorities = value.profilePriorities;
        if(_tmpProfilePriorities != null) {
          stmt.bindLong(5, _tmpProfilePriorities.a2dp_priority);
          stmt.bindLong(6, _tmpProfilePriorities.a2dp_sink_priority);
          stmt.bindLong(7, _tmpProfilePriorities.hfp_priority);
          stmt.bindLong(8, _tmpProfilePriorities.hfp_client_priority);
          stmt.bindLong(9, _tmpProfilePriorities.hid_host_priority);
          stmt.bindLong(10, _tmpProfilePriorities.pan_priority);
          stmt.bindLong(11, _tmpProfilePriorities.pbap_priority);
          stmt.bindLong(12, _tmpProfilePriorities.pbap_client_priority);
          stmt.bindLong(13, _tmpProfilePriorities.map_priority);
          stmt.bindLong(14, _tmpProfilePriorities.sap_priority);
          stmt.bindLong(15, _tmpProfilePriorities.hearing_aid_priority);
          stmt.bindLong(16, _tmpProfilePriorities.map_client_priority);
        } else {
          stmt.bindNull(5);
          stmt.bindNull(6);
          stmt.bindNull(7);
          stmt.bindNull(8);
          stmt.bindNull(9);
          stmt.bindNull(10);
          stmt.bindNull(11);
          stmt.bindNull(12);
          stmt.bindNull(13);
          stmt.bindNull(14);
          stmt.bindNull(15);
          stmt.bindNull(16);
        }
        final CustomizedMetadataEntity _tmpPublicMetadata = value.publicMetadata;
        if(_tmpPublicMetadata != null) {
          if (_tmpPublicMetadata.manufacturer_name == null) {
            stmt.bindNull(17);
          } else {
            stmt.bindBlob(17, _tmpPublicMetadata.manufacturer_name);
          }
          if (_tmpPublicMetadata.model_name == null) {
            stmt.bindNull(18);
          } else {
            stmt.bindBlob(18, _tmpPublicMetadata.model_name);
          }
          if (_tmpPublicMetadata.software_version == null) {
            stmt.bindNull(19);
          } else {
            stmt.bindBlob(19, _tmpPublicMetadata.software_version);
          }
          if (_tmpPublicMetadata.hardware_version == null) {
            stmt.bindNull(20);
          } else {
            stmt.bindBlob(20, _tmpPublicMetadata.hardware_version);
          }
          if (_tmpPublicMetadata.companion_app == null) {
            stmt.bindNull(21);
          } else {
            stmt.bindBlob(21, _tmpPublicMetadata.companion_app);
          }
          if (_tmpPublicMetadata.main_icon == null) {
            stmt.bindNull(22);
          } else {
            stmt.bindBlob(22, _tmpPublicMetadata.main_icon);
          }
          if (_tmpPublicMetadata.is_untethered_headset == null) {
            stmt.bindNull(23);
          } else {
            stmt.bindBlob(23, _tmpPublicMetadata.is_untethered_headset);
          }
          if (_tmpPublicMetadata.untethered_left_icon == null) {
            stmt.bindNull(24);
          } else {
            stmt.bindBlob(24, _tmpPublicMetadata.untethered_left_icon);
          }
          if (_tmpPublicMetadata.untethered_right_icon == null) {
            stmt.bindNull(25);
          } else {
            stmt.bindBlob(25, _tmpPublicMetadata.untethered_right_icon);
          }
          if (_tmpPublicMetadata.untethered_case_icon == null) {
            stmt.bindNull(26);
          } else {
            stmt.bindBlob(26, _tmpPublicMetadata.untethered_case_icon);
          }
          if (_tmpPublicMetadata.untethered_left_battery == null) {
            stmt.bindNull(27);
          } else {
            stmt.bindBlob(27, _tmpPublicMetadata.untethered_left_battery);
          }
          if (_tmpPublicMetadata.untethered_right_battery == null) {
            stmt.bindNull(28);
          } else {
            stmt.bindBlob(28, _tmpPublicMetadata.untethered_right_battery);
          }
          if (_tmpPublicMetadata.untethered_case_battery == null) {
            stmt.bindNull(29);
          } else {
            stmt.bindBlob(29, _tmpPublicMetadata.untethered_case_battery);
          }
          if (_tmpPublicMetadata.untethered_left_charging == null) {
            stmt.bindNull(30);
          } else {
            stmt.bindBlob(30, _tmpPublicMetadata.untethered_left_charging);
          }
          if (_tmpPublicMetadata.untethered_right_charging == null) {
            stmt.bindNull(31);
          } else {
            stmt.bindBlob(31, _tmpPublicMetadata.untethered_right_charging);
          }
          if (_tmpPublicMetadata.untethered_case_charging == null) {
            stmt.bindNull(32);
          } else {
            stmt.bindBlob(32, _tmpPublicMetadata.untethered_case_charging);
          }
          if (_tmpPublicMetadata.enhanced_settings_ui_uri == null) {
            stmt.bindNull(33);
          } else {
            stmt.bindBlob(33, _tmpPublicMetadata.enhanced_settings_ui_uri);
          }
        } else {
          stmt.bindNull(17);
          stmt.bindNull(18);
          stmt.bindNull(19);
          stmt.bindNull(20);
          stmt.bindNull(21);
          stmt.bindNull(22);
          stmt.bindNull(23);
          stmt.bindNull(24);
          stmt.bindNull(25);
          stmt.bindNull(26);
          stmt.bindNull(27);
          stmt.bindNull(28);
          stmt.bindNull(29);
          stmt.bindNull(30);
          stmt.bindNull(31);
          stmt.bindNull(32);
          stmt.bindNull(33);
        }
      }
    };
    this.__preparedStmtOfDelete = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM metadata WHERE address = ?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAll = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM metadata";
        return _query;
      }
    };
  }

  @Override
  public void insert(Metadata... metadata) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfMetadata.insert(metadata);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(String address) {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDelete.acquire();
    __db.beginTransaction();
    try {
      int _argIndex = 1;
      if (address == null) {
        _stmt.bindNull(_argIndex);
      } else {
        _stmt.bindString(_argIndex, address);
      }
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDelete.release(_stmt);
    }
  }

  @Override
  public void deleteAll() {
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAll.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAll.release(_stmt);
    }
  }

  @Override
  public List<Metadata> load() {
    final String _sql = "SELECT * FROM metadata";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfAddress = _cursor.getColumnIndexOrThrow("address");
      final int _cursorIndexOfMigrated = _cursor.getColumnIndexOrThrow("migrated");
      final int _cursorIndexOfA2dpSupportsOptionalCodecs = _cursor.getColumnIndexOrThrow("a2dpSupportsOptionalCodecs");
      final int _cursorIndexOfA2dpOptionalCodecsEnabled = _cursor.getColumnIndexOrThrow("a2dpOptionalCodecsEnabled");
      final int _cursorIndexOfA2dpPriority = _cursor.getColumnIndexOrThrow("a2dp_priority");
      final int _cursorIndexOfA2dpSinkPriority = _cursor.getColumnIndexOrThrow("a2dp_sink_priority");
      final int _cursorIndexOfHfpPriority = _cursor.getColumnIndexOrThrow("hfp_priority");
      final int _cursorIndexOfHfpClientPriority = _cursor.getColumnIndexOrThrow("hfp_client_priority");
      final int _cursorIndexOfHidHostPriority = _cursor.getColumnIndexOrThrow("hid_host_priority");
      final int _cursorIndexOfPanPriority = _cursor.getColumnIndexOrThrow("pan_priority");
      final int _cursorIndexOfPbapPriority = _cursor.getColumnIndexOrThrow("pbap_priority");
      final int _cursorIndexOfPbapClientPriority = _cursor.getColumnIndexOrThrow("pbap_client_priority");
      final int _cursorIndexOfMapPriority = _cursor.getColumnIndexOrThrow("map_priority");
      final int _cursorIndexOfSapPriority = _cursor.getColumnIndexOrThrow("sap_priority");
      final int _cursorIndexOfHearingAidPriority = _cursor.getColumnIndexOrThrow("hearing_aid_priority");
      final int _cursorIndexOfMapClientPriority = _cursor.getColumnIndexOrThrow("map_client_priority");
      final int _cursorIndexOfManufacturerName = _cursor.getColumnIndexOrThrow("manufacturer_name");
      final int _cursorIndexOfModelName = _cursor.getColumnIndexOrThrow("model_name");
      final int _cursorIndexOfSoftwareVersion = _cursor.getColumnIndexOrThrow("software_version");
      final int _cursorIndexOfHardwareVersion = _cursor.getColumnIndexOrThrow("hardware_version");
      final int _cursorIndexOfCompanionApp = _cursor.getColumnIndexOrThrow("companion_app");
      final int _cursorIndexOfMainIcon = _cursor.getColumnIndexOrThrow("main_icon");
      final int _cursorIndexOfIsUntetheredHeadset = _cursor.getColumnIndexOrThrow("is_untethered_headset");
      final int _cursorIndexOfUntetheredLeftIcon = _cursor.getColumnIndexOrThrow("untethered_left_icon");
      final int _cursorIndexOfUntetheredRightIcon = _cursor.getColumnIndexOrThrow("untethered_right_icon");
      final int _cursorIndexOfUntetheredCaseIcon = _cursor.getColumnIndexOrThrow("untethered_case_icon");
      final int _cursorIndexOfUntetheredLeftBattery = _cursor.getColumnIndexOrThrow("untethered_left_battery");
      final int _cursorIndexOfUntetheredRightBattery = _cursor.getColumnIndexOrThrow("untethered_right_battery");
      final int _cursorIndexOfUntetheredCaseBattery = _cursor.getColumnIndexOrThrow("untethered_case_battery");
      final int _cursorIndexOfUntetheredLeftCharging = _cursor.getColumnIndexOrThrow("untethered_left_charging");
      final int _cursorIndexOfUntetheredRightCharging = _cursor.getColumnIndexOrThrow("untethered_right_charging");
      final int _cursorIndexOfUntetheredCaseCharging = _cursor.getColumnIndexOrThrow("untethered_case_charging");
      final int _cursorIndexOfEnhancedSettingsUiUri = _cursor.getColumnIndexOrThrow("enhanced_settings_ui_uri");
      final List<Metadata> _result = new ArrayList<Metadata>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Metadata _item;
        final String _tmpAddress;
        _tmpAddress = _cursor.getString(_cursorIndexOfAddress);
        final ProfilePrioritiesEntity _tmpProfilePriorities;
        if (! (_cursor.isNull(_cursorIndexOfA2dpPriority) && _cursor.isNull(_cursorIndexOfA2dpSinkPriority) && _cursor.isNull(_cursorIndexOfHfpPriority) && _cursor.isNull(_cursorIndexOfHfpClientPriority) && _cursor.isNull(_cursorIndexOfHidHostPriority) && _cursor.isNull(_cursorIndexOfPanPriority) && _cursor.isNull(_cursorIndexOfPbapPriority) && _cursor.isNull(_cursorIndexOfPbapClientPriority) && _cursor.isNull(_cursorIndexOfMapPriority) && _cursor.isNull(_cursorIndexOfSapPriority) && _cursor.isNull(_cursorIndexOfHearingAidPriority) && _cursor.isNull(_cursorIndexOfMapClientPriority))) {
          _tmpProfilePriorities = new ProfilePrioritiesEntity();
          _tmpProfilePriorities.a2dp_priority = _cursor.getInt(_cursorIndexOfA2dpPriority);
          _tmpProfilePriorities.a2dp_sink_priority = _cursor.getInt(_cursorIndexOfA2dpSinkPriority);
          _tmpProfilePriorities.hfp_priority = _cursor.getInt(_cursorIndexOfHfpPriority);
          _tmpProfilePriorities.hfp_client_priority = _cursor.getInt(_cursorIndexOfHfpClientPriority);
          _tmpProfilePriorities.hid_host_priority = _cursor.getInt(_cursorIndexOfHidHostPriority);
          _tmpProfilePriorities.pan_priority = _cursor.getInt(_cursorIndexOfPanPriority);
          _tmpProfilePriorities.pbap_priority = _cursor.getInt(_cursorIndexOfPbapPriority);
          _tmpProfilePriorities.pbap_client_priority = _cursor.getInt(_cursorIndexOfPbapClientPriority);
          _tmpProfilePriorities.map_priority = _cursor.getInt(_cursorIndexOfMapPriority);
          _tmpProfilePriorities.sap_priority = _cursor.getInt(_cursorIndexOfSapPriority);
          _tmpProfilePriorities.hearing_aid_priority = _cursor.getInt(_cursorIndexOfHearingAidPriority);
          _tmpProfilePriorities.map_client_priority = _cursor.getInt(_cursorIndexOfMapClientPriority);
        }  else  {
          _tmpProfilePriorities = null;
        }
        final CustomizedMetadataEntity _tmpPublicMetadata;
        _tmpPublicMetadata = new CustomizedMetadataEntity();
        _tmpPublicMetadata.manufacturer_name = _cursor.getBlob(_cursorIndexOfManufacturerName);
        _tmpPublicMetadata.model_name = _cursor.getBlob(_cursorIndexOfModelName);
        _tmpPublicMetadata.software_version = _cursor.getBlob(_cursorIndexOfSoftwareVersion);
        _tmpPublicMetadata.hardware_version = _cursor.getBlob(_cursorIndexOfHardwareVersion);
        _tmpPublicMetadata.companion_app = _cursor.getBlob(_cursorIndexOfCompanionApp);
        _tmpPublicMetadata.main_icon = _cursor.getBlob(_cursorIndexOfMainIcon);
        _tmpPublicMetadata.is_untethered_headset = _cursor.getBlob(_cursorIndexOfIsUntetheredHeadset);
        _tmpPublicMetadata.untethered_left_icon = _cursor.getBlob(_cursorIndexOfUntetheredLeftIcon);
        _tmpPublicMetadata.untethered_right_icon = _cursor.getBlob(_cursorIndexOfUntetheredRightIcon);
        _tmpPublicMetadata.untethered_case_icon = _cursor.getBlob(_cursorIndexOfUntetheredCaseIcon);
        _tmpPublicMetadata.untethered_left_battery = _cursor.getBlob(_cursorIndexOfUntetheredLeftBattery);
        _tmpPublicMetadata.untethered_right_battery = _cursor.getBlob(_cursorIndexOfUntetheredRightBattery);
        _tmpPublicMetadata.untethered_case_battery = _cursor.getBlob(_cursorIndexOfUntetheredCaseBattery);
        _tmpPublicMetadata.untethered_left_charging = _cursor.getBlob(_cursorIndexOfUntetheredLeftCharging);
        _tmpPublicMetadata.untethered_right_charging = _cursor.getBlob(_cursorIndexOfUntetheredRightCharging);
        _tmpPublicMetadata.untethered_case_charging = _cursor.getBlob(_cursorIndexOfUntetheredCaseCharging);
        _tmpPublicMetadata.enhanced_settings_ui_uri = _cursor.getBlob(_cursorIndexOfEnhancedSettingsUiUri);
        _item = new Metadata(_tmpAddress);
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfMigrated);
        _item.migrated = _tmp != 0;
        _item.a2dpSupportsOptionalCodecs = _cursor.getInt(_cursorIndexOfA2dpSupportsOptionalCodecs);
        _item.a2dpOptionalCodecsEnabled = _cursor.getInt(_cursorIndexOfA2dpOptionalCodecsEnabled);
        _item.profilePriorities = _tmpProfilePriorities;
        _item.publicMetadata = _tmpPublicMetadata;
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
