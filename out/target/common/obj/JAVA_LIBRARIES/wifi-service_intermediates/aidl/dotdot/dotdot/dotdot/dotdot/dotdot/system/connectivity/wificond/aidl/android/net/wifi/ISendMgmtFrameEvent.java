/*
 * This file is auto-generated.  DO NOT MODIFY.
 */
package android.net.wifi;
// A callback to notify the results of sending a management frame.

public interface ISendMgmtFrameEvent extends android.os.IInterface
{
  /** Default implementation for ISendMgmtFrameEvent. */
  public static class Default implements android.net.wifi.ISendMgmtFrameEvent
  {
    // Called when the management frame was successfully sent and ACKed by the
    // recipient.
    // @param elapsedTimeMs The elapsed time between when the management frame was
    //     sent and when the ACK was processed, in milliseconds, as measured by
    //     wificond. This includes the time that the send frame spent queuing
    //     before it was sent, any firmware retries, and the time the received
    //     ACK spent queuing before it was processed.

    @Override public void OnAck(int elapsedTimeMs) throws android.os.RemoteException
    {
    }
    // Called when the send failed.
    // @param reason The error code for the failure.

    @Override public void OnFailure(int reason) throws android.os.RemoteException
    {
    }
    @Override
    public android.os.IBinder asBinder() {
      return null;
    }
  }
  /** Local-side IPC implementation stub class. */
  public static abstract class Stub extends android.os.Binder implements android.net.wifi.ISendMgmtFrameEvent
  {
    private static final java.lang.String DESCRIPTOR = "android.net.wifi.ISendMgmtFrameEvent";
    /** Construct the stub at attach it to the interface. */
    public Stub()
    {
      this.attachInterface(this, DESCRIPTOR);
    }
    /**
     * Cast an IBinder object into an android.net.wifi.ISendMgmtFrameEvent interface,
     * generating a proxy if needed.
     */
    public static android.net.wifi.ISendMgmtFrameEvent asInterface(android.os.IBinder obj)
    {
      if ((obj==null)) {
        return null;
      }
      android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
      if (((iin!=null)&&(iin instanceof android.net.wifi.ISendMgmtFrameEvent))) {
        return ((android.net.wifi.ISendMgmtFrameEvent)iin);
      }
      return new android.net.wifi.ISendMgmtFrameEvent.Stub.Proxy(obj);
    }
    @Override public android.os.IBinder asBinder()
    {
      return this;
    }
    @Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
    {
      java.lang.String descriptor = DESCRIPTOR;
      switch (code)
      {
        case INTERFACE_TRANSACTION:
        {
          reply.writeString(descriptor);
          return true;
        }
        case TRANSACTION_OnAck:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.OnAck(_arg0);
          return true;
        }
        case TRANSACTION_OnFailure:
        {
          data.enforceInterface(descriptor);
          int _arg0;
          _arg0 = data.readInt();
          this.OnFailure(_arg0);
          return true;
        }
        default:
        {
          return super.onTransact(code, data, reply, flags);
        }
      }
    }
    private static class Proxy implements android.net.wifi.ISendMgmtFrameEvent
    {
      private android.os.IBinder mRemote;
      Proxy(android.os.IBinder remote)
      {
        mRemote = remote;
      }
      @Override public android.os.IBinder asBinder()
      {
        return mRemote;
      }
      public java.lang.String getInterfaceDescriptor()
      {
        return DESCRIPTOR;
      }
      // Called when the management frame was successfully sent and ACKed by the
      // recipient.
      // @param elapsedTimeMs The elapsed time between when the management frame was
      //     sent and when the ACK was processed, in milliseconds, as measured by
      //     wificond. This includes the time that the send frame spent queuing
      //     before it was sent, any firmware retries, and the time the received
      //     ACK spent queuing before it was processed.

      @Override public void OnAck(int elapsedTimeMs) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(elapsedTimeMs);
          boolean _status = mRemote.transact(Stub.TRANSACTION_OnAck, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().OnAck(elapsedTimeMs);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      // Called when the send failed.
      // @param reason The error code for the failure.

      @Override public void OnFailure(int reason) throws android.os.RemoteException
      {
        android.os.Parcel _data = android.os.Parcel.obtain();
        try {
          _data.writeInterfaceToken(DESCRIPTOR);
          _data.writeInt(reason);
          boolean _status = mRemote.transact(Stub.TRANSACTION_OnFailure, _data, null, android.os.IBinder.FLAG_ONEWAY);
          if (!_status && getDefaultImpl() != null) {
            getDefaultImpl().OnFailure(reason);
            return;
          }
        }
        finally {
          _data.recycle();
        }
      }
      public static android.net.wifi.ISendMgmtFrameEvent sDefaultImpl;
    }
    static final int TRANSACTION_OnAck = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
    static final int TRANSACTION_OnFailure = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
    public static boolean setDefaultImpl(android.net.wifi.ISendMgmtFrameEvent impl) {
      if (Stub.Proxy.sDefaultImpl == null && impl != null) {
        Stub.Proxy.sDefaultImpl = impl;
        return true;
      }
      return false;
    }
    public static android.net.wifi.ISendMgmtFrameEvent getDefaultImpl() {
      return Stub.Proxy.sDefaultImpl;
    }
  }
  public static final int SEND_MGMT_FRAME_ERROR_UNKNOWN = 1;
  public static final int SEND_MGMT_FRAME_ERROR_MCS_UNSUPPORTED = 2;
  public static final int SEND_MGMT_FRAME_ERROR_NO_ACK = 3;
  public static final int SEND_MGMT_FRAME_ERROR_TIMEOUT = 4;
  public static final int SEND_MGMT_FRAME_ERROR_ALREADY_STARTED = 5;
  // Called when the management frame was successfully sent and ACKed by the
  // recipient.
  // @param elapsedTimeMs The elapsed time between when the management frame was
  //     sent and when the ACK was processed, in milliseconds, as measured by
  //     wificond. This includes the time that the send frame spent queuing
  //     before it was sent, any firmware retries, and the time the received
  //     ACK spent queuing before it was processed.

  public void OnAck(int elapsedTimeMs) throws android.os.RemoteException;
  // Called when the send failed.
  // @param reason The error code for the failure.

  public void OnFailure(int reason) throws android.os.RemoteException;
}
