/* AUTO-GENERATED FILE. DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found. It
 * should not be modified by hand.
 */

package android.auto_generated_rro_vendor__;

public final class R {
  public static final class array {
    public static final int config_ambientThresholdsOfPeakRefreshRate=0x7f010000;
    /**
     * List of files pinned by the Pinner Service with the apex boot image b/119800099
     */
    public static final int config_apexBootImagePinnerServiceFiles=0x7f010001;
    /**
     * Array of desired screen brightness in nits corresponding to the lux values
     * in the config_autoBrightnessLevels array. As with config_screenBrightnessMinimumNits and
     * config_screenBrightnessMaximumNits, the display brightness is defined as the measured
     * brightness of an all-white image.
     * If this is defined then:
     * - config_autoBrightnessLcdBacklightValues should not be defined
     * - config_screenBrightnessNits must be defined
     * - config_screenBrightnessBacklight must be defined
     * This array should have size one greater than the size of the config_autoBrightnessLevels
     * array. The brightness values must be non-negative and non-decreasing. This must be
     * overridden in platform specific overlays
     */
    public static final int config_autoBrightnessDisplayValuesNits=0x7f010002;
    /**
     * Array of light sensor LUX values to define our levels for auto backlight brightness support.
     * The N entries of this array define N  1 zones as follows:
     * Zone 0:        0 <= LUX < array[0]
     * Zone 1:        array[0] <= LUX < array[1]
     * ...
     * Zone N:        array[N - 1] <= LUX < array[N]
     * Zone N + 1     array[N] <= LUX < infinity
     * Must be overridden in platform specific overlays
     */
    public static final int config_autoBrightnessLevels=0x7f010003;
    /**
     * List supported color modes.
     */
    public static final int config_availableColorModes=0x7f010004;
    /**
     * Not allow to switch to higher refresh rate when (display, ambient) brightness falls into
     * the region defined by the two arrays
     */
    public static final int config_brightnessThresholdsOfPeakRefreshRate=0x7f010005;
    /**
     * Default list of files pinned by the Pinner Service
     */
    public static final int config_defaultPinnerServiceFiles=0x7f010006;
    /**
     * The following two arrays specify which color space to use for display composition when a
     * certain color mode is active.
     * Composition color spaces are defined in android.view.Display.COLOR_MODE_xxx, and color
     * modes are defined in ColorDisplayManager.COLOR_MODE_xxx and
     * ColorDisplayManager.VENDOR_COLOR_MODE_xxx.
     * The color space COLOR_MODE_DEFAULT (0) lets the system select the most appropriate
     * composition color space for currently displayed content. Other values (e.g.,
     * COLOR_MODE_SRGB) override system selection; these other color spaces must be supported by
     * the device for for display composition.
     * If a color mode does not have a corresponding color space specified in this array, the
     * currently set composition color space will not be modified.
     */
    public static final int config_displayCompositionColorModes=0x7f010007;
    public static final int config_displayCompositionColorSpaces=0x7f010008;
    /**
     * See DisplayWhiteBalanceController.
     * A float array containing a list of ambient color temperatures, in Kelvin. This array,
     * together with config_displayWhiteBalanceDisplayColorTemperatures, is used to generate a
     * lookup table used in DisplayWhiteBalanceController. This lookup table is used to map
     * ambient color temperature readings to a target color temperature for the display.
     * This table is optional. If used, this array must,
     * 1) Contain at least two entries
     * 2) Be the same length as config_displayWhiteBalanceDisplayColorTemperatures.
     */
    public static final int config_displayWhiteBalanceAmbientColorTemperatures=0x7f010009;
    /**
     * See DisplayWhiteBalanceThrottler.
     * The ambient color temperature values used to determine the threshold as the corresponding
     * value in config_displayWhiteBalance{Increase,Decrease}Threholds. Must be non-empty, the
     * same length as config_displayWhiteBalance{Increase,Decrease}Thresholds, and contain
     * non-negative, strictly increasing numbers.
     * For example, if:
     * - baseThresolds = [0, 100, 1000];
     * - increaseThresholds = [0.1, 0.15, 0.2];
     * - decreaseThresholds = [0.1, 0.05, 0.0];
     * Then, given the ambient color temperature INCREASED from X to Y (so X < Y):
     * - If 0 <= Y < 100, we require Y > (1 + 0.1) * X = 1.1X;
     * - If 100 <= Y < 1000, we require Y > (1 + 0.15) * X = 1.15X;
     * - If 1000 <= Y, we require Y > (1 + 0.2) * X = 1.2X.
     * Or, if the ambient color temperature DECREASED from X to Y (so X > Y):
     * - If 0 <= Y < 100, we require Y < (1 - 0.1) * X = 0.9X;
     * - If 100 <= Y < 1000, we require Y < (1 - 0.05) * X = 0.95X;
     * - If 1000 <= Y, we require Y < (1 - 0) * X = X.
     */
    public static final int config_displayWhiteBalanceBaseThresholds=0x7f01000a;
    /**
     * See DisplayWhiteBalanceThrottler.
     * The decrease threshold values, throttled if value > value * (1 - threshold). Must be
     * non-empty, the same length as config_displayWhiteBalanceBaseThresholds, and contain
     * non-negative numbers.
     */
    public static final int config_displayWhiteBalanceDecreaseThresholds=0x7f01000b;
    /**
     * See DisplayWhiteBalanceController.
     * An array containing a list of display color temperatures, in Kelvin. See
     * config_displayWhiteBalanceAmbientColorTemperatures for additional details.
     * The same restrictions apply to this array.
     */
    public static final int config_displayWhiteBalanceDisplayColorTemperatures=0x7f01000c;
    /**
     * The nominal white coordinates, in CIE1931 XYZ color space, for Display White Balance to
     * use in its calculations. AWB will adapt this white point to the target ambient white
     * point. The array must include a total of 3 float values (X, Y, Z)
     */
    public static final int config_displayWhiteBalanceDisplayNominalWhite=0x7f01000d;
    /**
     * See DisplayWhiteBalanceController.
     * An array containing a list of biases. See
     * config_displayWhiteBalanceHighLightAmbientBrightnesses for additional details.
     * This array must be in the range of [0.0, 1.0].
     */
    public static final int config_displayWhiteBalanceHighLightAmbientBiases=0x7f01000e;
    /**
     * See DisplayWhiteBalanceController.
     * A float array containing a list of ambient brightnesses, in Lux. This array,
     * together with config_displayWhiteBalanceHighLightAmbientBiases, is used to generate a
     * lookup table used in DisplayWhiteBalanceController. This lookup table is used to map
     * ambient brightness readings to a bias, where the bias is used to linearly interpolate
     * between ambient color temperature and
     * config_displayWhiteBalanceHighLightAmbientColorTemperature.
     * This table is optional. If used, this array must,
     * 1) Contain at least two entries
     * 2) Be the same length as config_displayWhiteBalanceHighLightAmbientBiases.
     */
    public static final int config_displayWhiteBalanceHighLightAmbientBrightnesses=0x7f01000f;
    /**
     * See DisplayWhiteBalanceThrottler.
     * The increase threshold values, throttled if value < value * (1 + threshold). Must be
     * non-empty, the same length as config_displayWhiteBalanceBaseThresholds, and contain
     * non-negative numbers.
     */
    public static final int config_displayWhiteBalanceIncreaseThresholds=0x7f010010;
    /**
     * See DisplayWhiteBalanceController.
     * An array containing a list of biases. See
     * config_displayWhiteBalanceLowLightAmbientBrightnesses for additional details.
     * This array must be in the range of [0.0, 1.0].
     */
    public static final int config_displayWhiteBalanceLowLightAmbientBiases=0x7f010011;
    /**
     * See DisplayWhiteBalanceController.
     * A float array containing a list of ambient brightnesses, in Lux. This array,
     * together with config_displayWhiteBalanceLowLightAmbientBiases, is used to generate a
     * lookup table used in DisplayWhiteBalanceController. This lookup table is used to map
     * ambient brightness readings to a bias, where the bias is used to linearly interpolate
     * between ambient color temperature and
     * config_displayWhiteBalanceLowLightAmbientColorTemperature.
     * This table is optional. If used, this array must,
     * 1) Contain at least two entries
     * 2) Be the same length as config_displayWhiteBalanceLowLightAmbientBiases.
     */
    public static final int config_displayWhiteBalanceLowLightAmbientBrightnesses=0x7f010012;
    /**
     * Display 90Hz blacklist. Apps that do not work well when the display refreshes at 90Hz.
     * When these apps are visible, the display's refresh rate gets fixed to 60Hz.
     */
    public static final int config_highRefreshRateBlacklist=0x7f010013;
    /**
     * Entitlement APP provisioning for Tethering
     */
    public static final int config_mobile_hotspot_provision_app=0x7f010014;
    /**
     * Configure mobile tcp buffer sizes in the form:
     * rat-name:rmem_min,rmem_def,rmem_max,wmem_min,wmem_def,wmem_max
     * If no value is found for the rat-name in use, the system default will be applied.
     */
    public static final int config_mobile_tcp_buffers=0x7f010015;
    /**
     * An array describing the screen's backlight values corresponding to the brightness
     * values in the config_screenBrightnessNits array.
     * This array should be equal in size to config_screenBrightnessBacklight.
     */
    public static final int config_screenBrightnessBacklight=0x7f010016;
    /**
     * An array of floats describing the screen brightness in nits corresponding to the backlight
     * values in the config_screenBrightnessBacklight array.  On OLED displays these  values
     * should be measured with an all white image while the display is in the fully on state.
     * Note that this value should *not* reflect the maximum brightness value for any high
     * brightness modes but only the maximum brightness value obtainable in a sustainable manner.
     * This array should be equal in size to config_screenBrightnessBacklight
     */
    public static final int config_screenBrightnessNits=0x7f010017;
    /**
     * An array of device capabilities defined by GSMA SGP.22 v2.0, and their corresponding major
     * version.
     */
    public static final int config_telephonyEuiccDeviceCapabilities=0x7f010018;
    /**
     * List of regexpressions describing the interface (if any) that represent tetherable
     * bluetooth interfaces.  If the device doesn't want to support tethering over bluetooth this
     * should be empty.
     */
    public static final int config_tether_bluetooth_regexs=0x7f010019;
    /**
     * List of regexpressions describing the interface (if any) that represent tetherable
     * USB interfaces.  If the device doesn't want to support tething over USB this should
     * be empty.  An example would be "usb.*"
     */
    public static final int config_tether_usb_regexs=0x7f01001a;
    /**
     * List of regexpressions describing the interface (if any) that represent tetherable
     * Wifi interfaces.  If the device doesn't want to support tethering over Wifi this
     * should be empty.  An example would be "softap.*"
     */
    public static final int config_tether_wifi_regexs=0x7f01001b;
    /**
     * Array indicating wifi fatal firmware alert error code list from driver
     */
    public static final int config_wifi_fatal_firmware_alert_error_code_list=0x7f01001c;
    /**
     * the 6th element indicates boot-time dependency-met value.
     */
    public static final int networkAttributes=0x7f01001d;
    /**
     * Device-specific array of SIM slot indexes which are are embedded eUICCs.
     * e.g. If a device has two physical slots with indexes 0, 1, and slot 1 is an
     * eUICC, then the value of this array should be:
     * <integer-array name="non_removable_euicc_slots">
     * <item>1</item>
     * </integer-array>
     * If a device has three physical slots and slot 1 and 2 are eUICCs, then the value of
     * this array should be:
     * <integer-array name="non_removable_euicc_slots">
     * <item>1</item>
     * <item>2</item>
     * </integer-array>
     * This is used to differentiate between removable eUICCs and built in eUICCs, and should
     * be set by OEMs for devices which use eUICCs.
     */
    public static final int non_removable_euicc_slots=0x7f01001e;
    /**
     * An Array of "[ConnectivityManager connectionType],
     * [# simultaneous connection types]"
     */
    public static final int radioAttributes=0x7f01001f;
  }
  public static final class bool {
    /**
     * Use ERI text for network name on CDMA LTE
     */
    public static final int config_LTE_eri_for_network_name=0x7f020000;
    /**
     * Flag indicating whether the we should enable the automatic brightness in Settings.
     * Software implementation will be used if config_hardware_auto_brightness_available is not set
     */
    public static final int config_automatic_brightness_available=0x7f020001;
    /**
     * Whether or not we should show the option to show battery percentage
     */
    public static final int config_battery_percentage_setting_available=0x7f020002;
    /**
     * Boolean indicating if current platform supports HFP inband ringing
     */
    public static final int config_bluetooth_hfp_inband_ringing_support=0x7f020003;
    /**
     * Set to true to add links to Cell Broadcast app from Settings and MMS app.
     */
    public static final int config_cellBroadcastAppLinks=0x7f020004;
    /**
     * Flag specifying whether VoLTE is availasble on device
     */
    public static final int config_device_volte_available=0x7f020005;
    /**
     * Flag specifying whether VoLTE is available on device
     */
    public static final int config_device_vt_available=0x7f020006;
    /**
     * Flag specifying whether WFC over IMS is availasble on device
     */
    public static final int config_device_wfc_ims_available=0x7f020007;
    /**
     * True if the display hardware only has brightness buckets rather than a full range of
     * backlight values
     */
    public static final int config_displayBrightnessBucketsInDoze=0x7f020008;
    /**
     * Boolean indicating whether display white balance is supported.
     */
    public static final int config_displayWhiteBalanceAvailable=0x7f020009;
    /**
     * Boolean indicating whether display white balance should be enabled by default.
     */
    public static final int config_displayWhiteBalanceEnabledDefault=0x7f02000a;
    /**
     * Whether the always on display mode is available.
     */
    public static final int config_dozeAlwaysOnDisplayAvailable=0x7f02000b;
    /**
     * Disable AOD by default
     */
    public static final int config_dozeAlwaysOnEnabled=0x7f02000c;
    /**
     * If device supports pickup/lift gesture
     */
    public static final int config_dozePulsePickup=0x7f02000d;
    /**
     * If device has a sensor that can wake-up the lock screen
     */
    public static final int config_dozeWakeLockScreenSensorAvailable=0x7f02000e;
    /**
     * Flag specifying whether or not IMS will use the ImsResolver dynamically
     */
    public static final int config_dynamic_bind_ims=0x7f02000f;
    /**
     * Enables or disables haptic effect when the text insertion/selection handle is moved
     * manually by the user. Off by default, since the expected haptic feedback may not be
     * available on some devices.
     */
    public static final int config_enableHapticTextHandle=0x7f020010;
    /**
     * Whether Multiuser UI should be shown
     */
    public static final int config_enableMultiUserUI=0x7f020011;
    /**
     * Whether the new Auto Selection Network UI should be shown
     */
    public static final int config_enableNewAutoSelectNetworkUI=0x7f020012;
    /**
     * Whether Hearing Aid profile is supported
     */
    public static final int config_hearing_aid_profile_supported=0x7f020013;
    /**
     * Is the device capable of hot swapping an UICC Card
     */
    public static final int config_hotswapCapable=0x7f020014;
    /**
     * Indicate whether closing the lid causes the device to go to sleep and opening
     * it causes the device to wake up.
     * The default is false.
     */
    public static final int config_lidControlsSleep=0x7f020015;
    /**
     * Enable Night display, which requires HWC 2.0.
     */
    public static final int config_nightDisplayAvailable=0x7f020016;
    /**
     * Should the pinner service pin the Camera application?
     */
    public static final int config_pinnerCameraApp=0x7f020017;
    /**
     * Should the pinner service pin the Home application?
     */
    public static final int config_pinnerHomeApp=0x7f020018;
    /**
     * Specifies whether to decouple the auto-suspend state of the device from the display on/off state.
     */
    public static final int config_powerDecoupleAutoSuspendModeFromDisplay=0x7f020019;
    /**
     * Specifies whether to decouple the interactive state of the device from the display on/off state.
     */
    public static final int config_powerDecoupleInteractiveModeFromDisplay=0x7f02001a;
    /**
     * Boolean indicating whether the HWC setColorTransform function can be performed efficiently
     * in hardware.
     */
    public static final int config_setColorTransformAccelerated=0x7f02001b;
    /**
     * Whether a software navigation bar should be shown. NOTE: in the future this may be
     * autodetected from the Configuration.
     */
    public static final int config_showNavigationBar=0x7f02001c;
    /**
     * Config determines whether to update phone object when voice registration
     * state changes. Voice radio tech change will always trigger an update of
     * phone object irrespective of this config
     */
    public static final int config_switch_phone_on_voice_reg_state_change=0x7f02001d;
    public static final int config_tether_upstream_automatic=0x7f02001e;
    /**
     * If this is true, the screen will come on when you unplug usb/power/whatever.
     */
    public static final int config_unplugTurnsOnScreen=0x7f02001f;
    /**
     * Enable video pause workaround when enabling/disabling the camera.
     */
    public static final int config_useVideoPauseWorkaround=0x7f020020;
    /**
     * Boolean indicating whether the wifi chipset has background scan support
     */
    public static final int config_wifi_background_scan_support=0x7f020021;
    /**
     * Wifi driver supports batched scan
     */
    public static final int config_wifi_batched_scan_supported=0x7f020022;
    /**
     * True if the firmware supports connected MAC randomization
     */
    public static final int config_wifi_connected_mac_randomization_supported=0x7f020023;
    /**
     * converted from 5GHz to ANY due to hardware restrictions
     */
    public static final int config_wifi_convert_apband_5ghz_to_any=0x7f020024;
    /**
     * Boolean indicating whether the wifi chipset has dual frequency band support
     */
    public static final int config_wifi_dual_band_support=0x7f020025;
    /**
     * Boolean indicating whether 802.11r Fast BSS Transition is enabled on this platform
     */
    public static final int config_wifi_fast_bss_transition_enabled=0x7f020026;
    /**
     * True if the firmware supports Wi-Fi link probing
     */
    public static final int config_wifi_link_probing_supported=0x7f020027;
    /**
     * True if the firmware supports p2p MAC randomization
     */
    public static final int config_wifi_p2p_mac_randomization_supported=0x7f020028;
    /**
     * Boolean indicating whether or not to revert to default country code when cellular
     * radio is unable to find any MCC information to infer wifi country code from
     */
    public static final int config_wifi_revert_country_code_on_cellular_loss=0x7f020029;
    /**
     * Enable ACS (auto channel selection) for Wifi hotspot (SAP)
     */
    public static final int config_wifi_softap_acs_supported=0x7f02002a;
    /**
     * Enable 802.11ac for Wifi hotspot (SAP)
     */
    public static final int config_wifi_softap_ieee80211ac_supported=0x7f02002b;
    /**
     * Boolean indicating whether or not wifi should turn off when emergency call is made
     */
    public static final int config_wifi_turn_off_during_emergency_call=0x7f02002c;
    /**
     * Disables compatibility WAL mode.
     * In this mode, only database journal mode will be changed, connection pool
     * size will still be limited to a single connection.
     */
    public static final int db_compatibility_wal_supported=0x7f02002d;
    /**
     * The restoring is handled by modem if it is true
     */
    public static final int skip_restoring_network_selection=0x7f02002e;
  }
  public static final class dimen {
    /**
     * See DisplayWhiteBalanceController.
     * The ambient color temperature (in cct) to which we interpolates towards to when the
     * ambient brightness is within the high light range, see
     * config_displayWhiteBalanceHighLightAmbientBrightnesses.
     */
    public static final int config_displayWhiteBalanceHighLightAmbientColorTemperature=0x7f030000;
    /**
     * See DisplayWhiteBalanceController.
     * The ambient color temperature (in cct) to which we interpolates towards to when the
     * ambient brightness is within the low light range, see
     * config_displayWhiteBalanceLowLightAmbientBrightnesses.
     */
    public static final int config_displayWhiteBalanceLowLightAmbientColorTemperature=0x7f030001;
    /**
     * Radius of the software rounded corners.
     */
    public static final int rounded_corner_radius=0x7f030002;
    /**
     * Adjustment for software rounded corners since corners aren't perfectly round.
     */
    public static final int rounded_corner_radius_adjustment=0x7f030003;
    /**
     * Height of the status bar
     */
    public static final int status_bar_height_portrait=0x7f030004;
  }
  public static final class integer {
    /**
     * Color mode to use when accessibility transforms are enabled. This color mode must be
     * supported by the device, but not necessarily appear in config_availableColorModes. The
     * regularly selected color mode will be used if this value is negative.
     */
    public static final int config_accessibilityColorMode=0x7f040000;
    /**
     * Stability requirements in milliseconds for accepting a new brightness level.  This is used
     * for debouncing the light sensor.  Different constants are used to debounce the light sensor
     * when adapting to brighter or darker environments.  This parameter controls how quickly
     * brightness changes occur in response to an observed change in light level that exceeds the
     * hysteresis threshold.
     */
    public static final int config_autoBrightnessBrighteningLightDebounce=0x7f040001;
    public static final int config_autoBrightnessDarkeningLightDebounce=0x7f040002;
    /**
     * The default intensity level for haptic feedback. See
     * Settings.System.HAPTIC_FEEDBACK_INTENSITY more details on the constant values and
     * meanings.
     */
    public static final int config_defaultHapticFeedbackIntensity=0x7f040003;
    /**
     * The default intensity level for notification vibrations. See
     * Settings.System.NOTIFICATION_VIBRATION_INTENSITY more details on the constant values and
     * meanings.
     */
    public static final int config_defaultNotificationVibrationIntensity=0x7f040004;
    /**
     * The default peak refresh rate for a given device. Change this value if you want to allow
     * for higher refresh rates to be automatically used out of the box
     */
    public static final int config_defaultPeakRefreshRate=0x7f040005;
    /**
     * default refresh rate in the zone defined by birghtness and ambient thresholds
     */
    public static final int config_defaultRefreshRateInZone=0x7f040006;
    /**
     * The default intensity level for notification vibrations. See
     * Settings.System.RING_VIBRATION_INTENSITY more details on the constant values and
     * meanings.
     */
    public static final int config_defaultRingVibrationIntensity=0x7f040007;
    /**
     * Default color temperature, in Kelvin, used by display white balance.
     */
    public static final int config_displayWhiteBalanceColorTemperatureDefault=0x7f040008;
    /**
     * Maximum color temperature, in Kelvin, supported by display white balance.
     */
    public static final int config_displayWhiteBalanceColorTemperatureMax=0x7f040009;
    /**
     * Minimum color temperature, in Kelvin, supported by display white balance.
     */
    public static final int config_displayWhiteBalanceColorTemperatureMin=0x7f04000a;
    /**
     * See DisplayWhiteBalanceThrottler.
     * The debounce time (in milliseconds) for decreasing the screen color tempearture, throttled
     * if time < lastTime - debounce. Must be non-negative.
     */
    public static final int config_displayWhiteBalanceDecreaseDebounce=0x7f04000b;
    /**
     * See DisplayWhiteBalanceThrottler.
     * The debounce time (in milliseconds) for increasing the screen color temperature, throttled
     * if time > lastTime + debounce. Must be non-negative.
     */
    public static final int config_displayWhiteBalanceIncreaseDebounce=0x7f04000c;
    /**
     * User activity timeout: Minimum screen off timeout in milliseconds.
     * Sets a lower bound for the {@link Settings.System#SCREEN_OFF_TIMEOUT} setting
     * which determines how soon the device will go to sleep when there is no
     * user activity.
     * This value must be greater than zero, otherwise the device will immediately
     * fall asleep again as soon as it is awoken.
     */
    public static final int config_minimumScreenOffTimeout=0x7f04000d;
    /**
     * Maximum number of supported users
     */
    public static final int config_multiuserMaximumUsers=0x7f04000e;
    /**
     * If the hardware supports specially marking packets that caused a wakeup of the
     * main CPU, set this value to the mark used.
     */
    public static final int config_networkWakeupPacketMark=0x7f04000f;
    /**
     * Mask to use when checking skb mark defined in config_networkWakeupPacketMark above.
     */
    public static final int config_networkWakeupPacketMask=0x7f040010;
    /**
     * Default color temperature, in Kelvin, to tint the screen when Night display is
     * activated.
     */
    public static final int config_nightDisplayColorTemperatureDefault=0x7f040011;
    /**
     * Number of physical SIM slots on the device. This includes both eSIM and pSIM slots, and
     * is not necessarily the same as the number of phones/logical modems supported by the device.
     * For example, a multi-sim device can have 2 phones/logical modems, but 3 physical slots,
     * or a single SIM device can have 1 phones/logical modems, but 2 physical slots (one eSIM
     * and one pSIM)
     */
    public static final int config_num_physical_slots=0x7f040012;
    /**
     * Minimum screen brightness allowed by the power manager.
     */
    public static final int config_screenBrightnessDim=0x7f040013;
    /**
     * Screen brightness when dozing.
     */
    public static final int config_screenBrightnessDoze=0x7f040014;
    /**
     * 8 bit brightness level of 6 corresponds to the 10 bit brightness level of 0x0B6,
     * 8 bit brightness level of 7 corresponds to the 10 bit brightness level of 0x0C1 on EVT1.1.
     */
    public static final int config_screenBrightnessForVrSettingDefault=0x7f040015;
    public static final int config_screenBrightnessForVrSettingMaximum=0x7f040016;
    public static final int config_screenBrightnessForVrSettingMinimum=0x7f040017;
    /**
     * Default screen brightness setting.
     * Must be in the range specified by minimum and maximum.
     */
    public static final int config_screenBrightnessSettingDefault=0x7f040018;
    /**
     * Maximum screen brightness setting allowed by the power manager.
     * The user is forbidden from setting the brightness above this level.
     */
    public static final int config_screenBrightnessSettingMaximum=0x7f040019;
    /**
     * Minimum screen brightness setting allowed by the power manager.
     * The user is forbidden from setting the brightness below this level.
     */
    public static final int config_screenBrightnessSettingMinimum=0x7f04001a;
    /**
     * Shutdown if the battery temperature exceeds (this value * 0.1) Celsius.
     */
    public static final int config_shutdownBatteryTemperature=0x7f04001b;
    /**
     * Rx current for wifi radio. 0 by default
     */
    public static final int config_wifi_active_rx_cur_ma=0x7f04001c;
    /**
     * Integer delay in milliseconds before set wlan interface up during watchdog recovery
     */
    public static final int config_wifi_framework_recovery_timeout_delay=0x7f04001d;
    public static final int config_wifi_framework_wifi_score_bad_rssi_threshold_24GHz=0x7f04001e;
    /**
     * Integer thresholds for low network score, should be somewhat less than the entry threshholds
     */
    public static final int config_wifi_framework_wifi_score_bad_rssi_threshold_5GHz=0x7f04001f;
    public static final int config_wifi_framework_wifi_score_entry_rssi_threshold_24GHz=0x7f040020;
    /**
     * Integer thresholds, do not connect to APs with RSSI lower than these values
     */
    public static final int config_wifi_framework_wifi_score_entry_rssi_threshold_5GHz=0x7f040021;
    /**
     * Idle Receive current for wifi radio. 0 by default
     */
    public static final int config_wifi_idle_receive_cur_ma=0x7f040022;
    /**
     * Operating volatage for wifi radio. 0 by default
     */
    public static final int config_wifi_operating_voltage_mv=0x7f040023;
    /**
     * Tx current for wifi radio. 0 by default
     */
    public static final int config_wifi_tx_cur_ma=0x7f040024;
  }
  public static final class string {
    /**
     * Auto-brightness sensor type string
     */
    public static final int config_displayLightSensorType=0x7f050000;
    /**
     * Enable doze mode
     * ComponentName of a dream to show whenever the system would otherwise have gone to sleep.
     */
    public static final int config_dozeComponent=0x7f050001;
    /**
     * Type of the ambient tap sensor. Empty if ambient tap is not supported.
     */
    public static final int config_dozeTapSensorType=0x7f050002;
    /**
     * ImsService package name to bind to by default, if config_dynamic_bind_ims is true
     */
    public static final int config_ims_package=0x7f050003;
    /**
     * MMS user agent string
     */
    public static final int config_mms_user_agent=0x7f050004;
    /**
     * MMS user agent profile url
     */
    public static final int config_mms_user_agent_profile_url=0x7f050005;
    public static final int config_mobile_hotspot_provision_app_no_ui=0x7f050006;
    public static final int config_mobile_hotspot_provision_response=0x7f050007;
    public static final int config_qualified_networks_service_package=0x7f050008;
    /**
     * Config SoftAP 2G channel list
     */
    public static final int config_wifi_framework_sap_2G_channel_list=0x7f050009;
    /**
     * Configure wifi tcp buffersizes in the form:
     * rmem_min,rmem_def,rmem_max,wmem_min,wmem_def,wmem_max
     */
    public static final int config_wifi_tcp_buffers=0x7f05000a;
    public static final int config_wlan_data_service_package=0x7f05000b;
    public static final int config_wlan_network_service_package=0x7f05000c;
    /**
     * Do not translate. Default access point SSID used for tethering
     */
    public static final int wifi_tether_configure_ssid_default=0x7f05000d;
  }
  public static final class xml {
    public static final int power_profile=0x7f060000;
  }
}